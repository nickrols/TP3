// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
app.use(cookieParser());
var db = new sqlite3.Database('db.sqlite');
var shajs = require('sha.js');


// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
//app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/" , function(req, res, next) {
    db.all('SELECT token FROM sessions WHERE token=?', [req.cookies['token']], function(err, data) {
        if(data.length>0)
        {
            //res.json({ statues: "yolo" });
            res.sendFile(__dirname + '/public/deconnexion.html');
        }
        else
        {
            res.sendFile(__dirname + '/public/index.html');
        }
    });
});

app.post("/login", function (req, res, next) {
    // Generate a 16 character alpha-numeric token:
    
    db.all('SELECT rowid, ident, password FROM users WHERE ident=? AND password=?;', [req.body.user, req.body.mdp], function (err, data) {
        
        if (data.length > 0) {
            //res.json({ statues: req.cookie });
            console.log(req.cookies['token']);
            //console.log('yolo');
            db.all('SELECT token FROM sessions WHERE ident=? AND token=?', [req.body.user, req.cookies['token']], function (err, data2) {
                console.log(data2.length);
                if (err) {
                    console.error(err);
                }

                if (data2.length > 0) {
                    /*if (date2 == res.cookie)
                    {
                        console.log("")
                    }*/
                    //res.json({ statues: "yolo" });
                    res.sendFile(__dirname + '/public/deconnexion.html');
                    //db.all('UPDATE sessions SET token=? WHERE ident=?;', ["", req.body.user]);

                }
                else
                {
                    //res.json({ statues: false });
                    
                    var token = shajs('sha256').update(Date.now() + req.body.user + Math.random()).digest('hex');

                    //db.all('INSERT INTO sessions VALUES(?,?);', [req.body.user, token]);
                    db.all('UPDATE sessions SET token=? WHERE ident=?;', [token, req.body.user]);
                    //console.log(token);
                    //db.all('DELETE FROM sessions WHERE ident=?', [req.body.user]),function (err, data){}
                    res.cookie("token", token);
                    //console.log(req.cookies);

                   // res.json({ statues: true, token: token, date: Date.now(), cookie: res.cookie });
                    res.sendFile(__dirname + '/public/deconnexion.html');
                    
                }
            });
            
        }
        else {
            res.json({ statues: false });
        }
       
    });
    
});

app.post("/deco", function (req, res, next) {
    res.clearCookie('token');
    res.sendFile(__dirname + '/public/index.html');

});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
